const mergeK = require('./assignment');

test('Test case 1: seconds_batches -> sorted_uniq_seconds', () => {
  let arr = [[1, 2, 3, 4], [1, 2, 3, 4, 5, 6, 7], [30, 31, 32, 33], [1, 2, 3, 32, 33, 34, 35, 36, 37, 38, 39, 40]];
  let k = 4;
  expect(mergeK(arr, k)).toStrictEqual([1, 2, 3, 4, 5, 6, 7, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40]);
});


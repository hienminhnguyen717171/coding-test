/**
 * Please write code to change seconds_batches to be sorted_uniq_seconds in simplest way in Javascript

const seconds_batches = [
  [1, 2, 3, 4],
  [1, 2, 3, 4, 5, 6, 7],
  [30, 31, 32, 33],
  [1, 2, 3, 32, 33, 34, 35, 36, 37, 38, 39, 40],
]

const sorted_uniq_seconds = [1, 2, 3, 4, 5, 6, 7, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40]
 */

class Node {
    constructor(val, row, col) {
    this.val = val;
    this.row = row;
    this.col = col;
    }
}
     
class PriorityQueue {
    constructor() {
        this.items = [];
    }
     
     
    enqueue(node) {
        let contain = false;
        for (let i = 0; i < this.items.length; i++) {
            if (this.items[i].val > node.val) {
                this.items.splice(i, 0, node);
                contain = true;
                break;
            }
        }
        if (!contain) {
            this.items.push(node);
        }
    }
     
    dequeue() {
        return this.items.shift();
    }
     
    isEmpty() {
        return this.items.length == 0;
    }
}
     
/**
 * mergeK code to change seconds_batches to be sorted_uniq_seconds in simplest way in Javascript
 * @param {*} arr 
 * @param {*} k 
 * @returns 
 */
function mergeK(arr, k) {
    let sorted_uniq_seconds_set = new Set();
    let pq = new PriorityQueue();
     
    // 1. Inserting the first elements of each row
    for (let i = 0; i < arr.length; i++) {
        pq.enqueue(new Node(arr[i][0], i, 0));
    }
     
    // 2. Loop to merge all the arrays
    while (!pq.isEmpty()) {
        let node = pq.dequeue();
        sorted_uniq_seconds_set.add(node.val);
        if (node.col + 1 < arr[node.row].length) {
            pq.enqueue(new Node(arr[node.row][node.col + 1], node.row, node.col + 1));
        }
    }
    const sorted_uniq_seconds_array = Array.from(sorted_uniq_seconds_set);
    return sorted_uniq_seconds_array;
}

/**
 *Time Complexity: O(N*K*logK) 
  Space complexity: O(N)
 */

// Driver code
let arr = [[1, 2, 3, 4], [1, 2, 3, 4, 5, 6, 7], [30, 31, 32, 33], [1, 2, 3, 32, 33, 34, 35, 36, 37, 38, 39, 40]];
let k = 4;
let l = mergeK(arr, k);
console.log(l.join(' '))


module.exports = mergeK;